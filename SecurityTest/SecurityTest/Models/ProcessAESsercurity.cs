﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SecurityTest.Models
{
    public class ProcessAESsercurity
    {
        string key = "12345678901234567890123456789012";
        string iv = "1234567890abcdef";

        public string encrypt(string content)
        {
            try
            { 
                ////取得AES key和IV
                //AESKeyPair aeskeypair = new AESKeyPair();
                //key = aeskeypair.aeskey(0);//測試用
                //iv = aeskeypair.aeskey(1);

                byte[] sourceBytes = Encoding.UTF8.GetBytes(content);
                var aes = new RijndaelManaged();
                aes.Key = Encoding.UTF8.GetBytes(key);//根據傳值接key
                aes.IV = Encoding.UTF8.GetBytes(iv);//根據傳值接iv
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;

                ICryptoTransform transform = aes.CreateEncryptor();
                return Convert.ToBase64String(transform.TransformFinalBlock(sourceBytes, 0, sourceBytes.Length));
             }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        public string decrypt(string AppCipher)
        {
            try
            {
                ////取得AES key和IV
                //AESKeyPair aeskeypair = new AESKeyPair();
                //string aeskey = aeskeypair.aeskey(0);//測試用
                //string aesiv = aeskeypair.aeskey(1);

                var encryptBytes = Convert.FromBase64String(AppCipher);
                var aes = new RijndaelManaged();
                aes.Key = Encoding.UTF8.GetBytes(key);//根據傳值接key
                aes.IV = Encoding.UTF8.GetBytes(iv);//根據傳值接iv
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;

                ICryptoTransform transform = aes.CreateDecryptor();
                return Encoding.UTF8.GetString(transform.TransformFinalBlock(encryptBytes, 0, encryptBytes.Length));
             }
            catch(Exception e)
            {
                return e.ToString();
            }
            
        }
    }
}