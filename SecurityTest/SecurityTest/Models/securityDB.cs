﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace SecurityTest.Models
{   
    public class securityDB : DbContext
    {
        public securityDB()
            : base("securityDB")
        {

        }
        public DbSet<Exchange> items { get; set; }
    }
}