﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecurityTest.Models
{
    public class Exchange
    {
        public int id { get; set; }
        public string cipher { get; set; }
        public string content { get; set; }
    }
}