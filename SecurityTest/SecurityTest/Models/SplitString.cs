﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecurityTest.Models
{
    public class SplitString
    {
        public string splitString(string content, int startIndex,int stringLength)
        {
            try
            {
                //statrtIndex : 起始位置，stringLength:從startIndex 開始後的幾個位置
                return content.Substring(startIndex, stringLength);
            }
            catch (Exception e)
            {
                return "Length Error.";
            }
            
        }
    }
}