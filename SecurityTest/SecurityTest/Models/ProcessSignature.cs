﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SecurityTest.Models
{
    public class ProcessSignature
    {
        public bool check(string AppSignature, string AppContent, string AppPublicKey)
        {
            try
            {
                //方便測試，使用ServerPublicKey解密簽章
                //簽章使用明文
                byte[] dataOriginal = Encoding.UTF8.GetBytes(AppContent);//轉型成簽章使用的格式
                byte[] signedData = Convert.FromBase64String(AppSignature);

                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                RSAKeyPair keypair = new RSAKeyPair();
                //rsa.FromXmlString(keypair.rsakey(0));//使用Server public key 測試(之後抓取資料庫中的pubKey)
                rsa.FromXmlString(AppPublicKey);
                bool status = rsa.VerifyData(dataOriginal, new SHA256Managed(), signedData);

                return status;
            }
            catch (Exception e)
            {
                return false;
            }
           
        }

        public string generateSignature(string serverContent)
        {
            try
            {
                //方便測試，使用ServerPublicKey解密簽章
                //參數為明文
                RSAKeyPair keypair = new RSAKeyPair();
                string ServerPrivateKey = keypair.rsakey(1);//取得Private key
                byte[] dataToSign = Encoding.UTF8.GetBytes(serverContent);//轉型成簽章使用的格式

                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(keypair.rsakey(1));//使用Server private key 測試
                byte[] signature = rsa.SignData(dataToSign, new SHA256Managed());

                return Convert.ToBase64String(signature);//轉換成Base64方便顯示
            }
            catch(Exception e)
            {
                return e.ToString();
            }
            
        }
    }
}