﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using SecurityTest.Models;
using System.Security.Cryptography.X509Certificates;


namespace SecurityTest.Controllers
{    
    public class ValuesController : ApiController
    {
        string ServerPrivateKey = @"<RSAKeyValue><Modulus>tMm0dlzhUBwAW0rj8Xb+R6nzZ+NmKdqW+xQHM1pHS8sdcC7TdUQJL21mGHRG8pfKw+aQIGKR9vB/1TuklHz5aSgGDsU+f/CYtcJsnrv9zt2kiuW6Fy5Ch571QRYPXKz/8PKMNEM3mXUnjjJQuvzFGfOrfj0yzV6lY1nex5hWZdmJz8vtmC8CsFRj38NtHYRBZBTIbMFEbJQuwQuoyuLDpgUoI+2NytyNCvYX6rkuAb1j6IsIy++0HptNyFX0FePnEiQxfrd3LAav5UKdtqGE9ppKMMgqveyGN+T598Lzb3bsXarQ6noP9gnN+RfTa96Xna7YhxNfewJXFVjo22mnsQ==</Modulus><Exponent>AQAB</Exponent><P>9FT499hhG332sIjVRAsDpTMjYZYejWJq6SOyy8Bp+1kCVA8PzKUhc2NFlRz83XVLHWFffasZ1E2X5G2dI+O6y0dzbUb7R9ro8lUepGTkbnzbtSgYx/iRYpLvspJI1uaN7oKaPiOqwSepOnPSsz+fM0b+puh70lqDz2ggDcpgMz0=</P><Q>vWvjgeVpw9SDBHHV3giis2CEr32qquceZhnmWcWQF+zga6bXRY0T1WP9XImFgoSgE+HgC02RQzDyM784qAK+F6TuBZpZTc6uj9dW8WLx6GVs4lR0ySqQud3gd77KIdECOh09aShOyhx5gfxITgud/IxpDabzc+8cQ9DTYmW3vYU=</Q><DP>ZiPjlJCzP6/v4arCJNjJ5FS1dMUx7n73HJFc3EqKw5VZINTPEQjzK+6TFxvwJZA0H4smRT2y5YqudcLRD0DTRMmQ2lzzDP3Zpfype6XlzL6LhnbFrUsfU+sDI059/sCREuUsH8//QRIYb6Qdr+tn3SLPr+y+XpiSQ7FOvRUeFC0=</DP><DQ>gL+RTpwhLGMIfn/lTtptxCCJb0KqOymGZv2ZrVD0ZN6zl1g6n+KJJZSmBDzRC2qb54eyodwiLRpatcN2ffQHQsjuo85KfvFJgylpxf5r0HsH7DpdetI8tqjRxnJer2eB7QAs2on0K7n4+NoCR1HsG1lQml/sv9M5UfrhgtMCdYU=</DQ><InverseQ>d8oDq39wcnhyDQMJht8mZ+5pUuruXCIdNKdloTSWFZ2d8Tfh80PT9Sa02FNRDpnArskeD5h4MrYlfApbpwpr775MuNYLfAaE/HlMb7565GmAD11ClH953hVTh5O5TvCrQBgsQdUM1zgNNGF+8z8vm1XE5Zo6AZt/Lwec1QOs/fU=</InverseQ><D>J1mm/MNjLG1n23cdzcNFfiI9D34E0PPZ/I0DszcBgTsU7YPXtXTJXggdGpIGlkuZUBIZiV8DH+GBpY9Nl59rJysVxHXE0JK1HozS2SsYh8QhCjWOD3kbdIsSHWhYkCa8r7yPbVm7Gp0QLsh/X2/g0/EAgSH/comZDJq1XbLoDVNRnRCcIis9MnLEsitbO+rP/7vGHaih9aeqdmFAtslCCK68lcQ190hjGKxAFkVqi3PCX82i5UCQKdwNE8ttk8KinLh6hmXNKXebF1gTU7PoU/ZjZAzEPde0dedu+jxgIVKqhOEXND+LgnCF1dAXyY/wOCxLZR3T+PaHSEvS29FVtQ==</D></RSAKeyValue>";
        // GET api/values/5
        public string Get()
        {
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            RSA.FromXmlString(ServerPrivateKey);
            RSAtransfer aTrans = new RSAtransfer();
            string pemPubKey = aTrans.ExportPublicKey(RSA);
            string decode = aTrans.deCodePem(pemPubKey);

            return "Public key : " + RSA.ToXmlString(false) + "\r\n \r\n pemPubkey : " + pemPubKey + "\r\n \r\n decode :"+decode;
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
