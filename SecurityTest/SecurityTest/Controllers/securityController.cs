﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SecurityTest.Models;
using System.Security.Cryptography;

namespace SecurityTest.Controllers
{
    public class securityController : Controller
    {
        securityDB db = new securityDB();
        ProcessRSAsecurity rsaProcess = new ProcessRSAsecurity();
        ProcessAESsercurity aesProcess = new ProcessAESsercurity();
        ProcessSignature signatureProcess = new ProcessSignature();
        GenerateRandomString aRandom = new GenerateRandomString();
        SplitString aSplit = new SplitString();

        // GET: security
        public string Index()
        {
            Exchange item = new Exchange();
            string content = "this a test.";
            item.content = content;
            string result = Newtonsoft.Json.JsonConvert.SerializeObject(item);
            return result;
        }

        public string RSAencrypt(Exchange item)
        {            
            string result = "";
            result = rsaProcess.encrypt(item.content);            
            item.cipher = result;
            string content = Newtonsoft.Json.JsonConvert.SerializeObject(item);
            return content;
        }

        public string RSAdecrypt(Exchange item)
        {
            string result = "";
            result = rsaProcess.decrypt(item.cipher);
            item.content = result;
            string content = Newtonsoft.Json.JsonConvert.SerializeObject(item);
            return content;
        }

        public string AESencrypt(Exchange item)
        {
            string result = "";
            string content = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return content;
        }

        public string AESdecrypt(Exchange item)
        {
            string result = "";
            string content = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return content;
        }

        public string checkSig(Exchange item)
        {
            string result = "";
            return result;
        }

        public string generateSig(Exchange item)
        {
            string result = "";
            return result;
        }
    }
}