namespace SecurityTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Exchanges",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        cipher = c.String(),
                        rsakey = c.String(),
                        aeskey = c.String(),
                        aesiv = c.String(),
                        content = c.String(),
                        signature = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Exchanges");
        }
    }
}
